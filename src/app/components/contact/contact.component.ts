import {
  Component,
  OnInit
} from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
  submitClicked() {
    let clientName: string = ( < HTMLInputElement > document.getElementById("clientName")).value;
    let clientEmail: string = ( < HTMLInputElement > document.getElementById("clientEmail")).value;
    let clientWebsite: string = ( < HTMLInputElement > document.getElementById("clientWebsite")).value;
    let aboutMe: string = ( < HTMLInputElement > document.getElementById("aboutMe")).value;
    let clientBudget: string = ( < HTMLInputElement > document.getElementById("clientBudget")).value;
    let clientDuration: string = ( < HTMLInputElement > document.getElementById("clientDuration")).value;
    let projectDescription: string = ( < HTMLInputElement > document.getElementById("projectDescription")).value;
    //  let clientName:string =  (<HTMLInputElement>document.getElementById("clientName")).value;
    console.log(clientBudget + clientBudget + clientEmail + clientName + clientWebsite);
    
    if (clientName == "" || clientName.length < 3) {
       (<HTMLInputElement>document.getElementById("errorName")).innerHTML = "Enter a valid name. It must be of 4 characters or more";
    } else {
      (<HTMLInputElement>document.getElementById("successName")).innerHTML = "ok";
    }

    var pattern = 	
    /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/
    console.log(pattern.test(clientEmail));
    if (pattern.test(clientEmail) == false) {
      (<HTMLInputElement>document.getElementById("errorEmail")).innerHTML = "Please enter an address in a correct format";
    } else {
      
    }
    
  }
}
