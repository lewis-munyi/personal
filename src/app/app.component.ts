import {
  Component
} from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  today: number = Date.now();

  gotosite(url) {
    console.log(url);
    switch (url) {
      case 1:
        window.open('https://linkedin.com/lewismunyi', '_blank');
        break;
      case 2:
        window.open('https://twitter.com/LewisMunyi', '_blank');
        break;
      case 3:
        window.open('https://github.com/lewis-munyi', '_blank');
        break;
      case 4:
        window.open('https://bitbucket.org/lewis-munyi', '_blank');
        break;
      case 5:
        window.open('https://t.me/lewis_munyi', '_blank');
        break;
      case 6:
        window.open('mailto:lewis.munyi.97@gmail.com;', '_blank');
        break;
      case 7:
        window.open('https://plus.google.com/+lewismunyi', '_blank');
        break;
      case 8:
        window.open('https://skype.com/', '_blank');
        break;
      case 9:
        window.open('https://www.quora.com/profile/Lewis-Munyi', '_blank');
        break;
      case 10:
        window.open('https://instagram.com/lewis_munyi', '_blank');
        break;
      case 11:
        window.open('slack', '_blank');
        break;
      default:
        alert("error");
        break;
    }
  }
}
